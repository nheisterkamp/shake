import { lensPath, set } from 'ramda';

import { getPiece } from './getPiece';

export const move = (board, [fromRow, fromCol], [toRow, toCol]) =>
  set(
    lensPath([toRow, toCol]),
    getPiece(board, [fromRow, fromCol]),
    set(
      lensPath([fromRow, fromCol]),
      ' ',
      board
    )
  );
