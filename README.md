shake
===
Local multiplayer chess app.

To Do
---
- [ ] use TS/flow
- [ ] add tests (jest)
- [ ] add CI
- [ ] add CD

### Game Logic
- [ ] separate module for game logic
- [ ] define API
- [ ] support algebraic notation
- [ ] add chess variants (mkBoard)

### App
- [ ] add react-native-web
- [ ] save state
- [ ] ? use redux
- [ ] add settings

References
---
- https://github.com/niklasf/python-chess
