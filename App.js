import React from 'react';
import { Dimensions, TouchableHighlight } from 'react-native';
import styled from 'styled-components';

import { PieceIcon } from './src/Piece';
import {
  BLACK,
  WHITE,
  getPiece,
  mkBoard,
  move,
} from './src/chess';

const Container = styled.View`
  background-color: papayawhip;
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const Board = styled.View`
  background-color: papayawhip;
  justify-content: center;
  flex-direction: column;
`;

const Row = styled.Text`
  color: #c0f3f3;
  flex-direction: row;
`;

const Square = styled.View`
  align-items: center;
  justify-content: center;
`;

const PlayerText = styled.Text`

`

const initialState = {
  board: mkBoard(),
  player: WHITE,
  selected: null,
  undoStack: [],
  redoStack: [],
};

export const getSquareSize = () => {
  const { height, width } = Dimensions.get('window');

  return Math.floor((Math.min(height, width)) / 8);
};

export default class App extends React.Component {
  state = initialState

  componentDidMount () {
    this.syncSquareSize();
    Dimensions.addEventListener('change', this.syncSquareSize);
  }

  syncSquareSize = () => {
    this.setState({
      squareSize: getSquareSize(),
    });
  }

  onSquarePress = (pos) => {
    const { board, player, selected, undoStack } = this.state;
    const piece = getPiece(board, pos);

    if (selected) {
      const nextBoard = move(board, selected, pos);

      this.setState({
        board: nextBoard,
        player: !player,
        selected: null,
        undoStack: [...undoStack, nextBoard],
      });
    } else if (piece !== ' ' && (
      (player === WHITE && piece.toUpperCase() === piece) ||
      (player === BLACK && piece.toLowerCase() === piece)
    )) {
      this.setState({ selected: pos });
    }
  }

  render() {
    const { board, player, selected, squareSize } = this.state;
    const [selectedRow, selectedCol] = selected || [];
    const flipStyle = player
        ? {}
        : { transform: [{ rotate: '180deg' }] };

    return (
      <Container>
        <Board>
          {board.map((boardRow, row) => {
            return (
              <Row key={row}>
                {boardRow.map((piece, col) => {
                  const backgroundColor = ((row + col) % 2 === 0) ? '#ffce9e' : '#d18b47';
                  const selectedSquare = row === selectedRow && col === selectedCol;

                  return (
                    <TouchableHighlight
                      onPress={() => this.onSquarePress([row, col])}
                      key={col}
                      underlayColor="white">
                      <Square
                        width={squareSize}
                        height={squareSize}
                        style={{ backgroundColor: selectedSquare ? 'red' : backgroundColor }}>
                        <PieceIcon
                          width={squareSize}
                          height={squareSize}
                          piece={piece}
                          style={flipStyle}
                        />
                      </Square>
                    </TouchableHighlight>
                  );
                })}
              </Row>
            )
          }, board)}
        </Board>
        <PlayerText style={flipStyle}>
          PLAYER: {player ? 'WHITE' : 'BLACK'}
        </PlayerText>
      </Container>
    );
  }
}
